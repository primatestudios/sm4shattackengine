using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

//Based off of Kurogame Hammer's attack calculator for Sm4sh 4
public class Sm4shKHAttackEngine : AGeniousAttackEngine, IPercentable, IHitStunnable, IShieldable, IRollable, IKillable
{
    [Tooltip("Needs to have a .bytes suffix. For Instance: calculator64.exe.bytes")]
    public BinaryAsset binary;
    public override Type RewiredInputTypeDependency { get { return typeof(SmashInputManagerActions); } }
    protected readonly int m_HitStunned = Animator.StringToHash("HitStunned");
    protected readonly int m_DamagedFall = Animator.StringToHash("DamagedFall");

    public readonly int m_UpSpecial = Animator.StringToHash("UpSpecial");
    public readonly int m_DownSpecial = Animator.StringToHash("DownSpecial");
    public readonly int m_ForwardSpecial = Animator.StringToHash("ForwardSpecial");
    public readonly int m_DownSmash = Animator.StringToHash("DownSmash");
    public readonly int m_UpSmash = Animator.StringToHash("UpSmash");
    public readonly int m_ForwardSmash = Animator.StringToHash("ForwardSmash");
    public readonly int m_UpTilt = Animator.StringToHash("UpTilt");
    public readonly int m_DownTilt = Animator.StringToHash("DownTilt");
    public readonly int m_AttackDetected = Animator.StringToHash("AttackDetected");
    public readonly int m_AttackStarted = Animator.StringToHash("AttackStarted");
    public bool hitStunAerialCancellable { get; private set; }// = true;
    public bool hitStunAirDodgeCancellable { get; private set; }// = true;
    public float percent {get; protected set;}
    bool damagedFall
    {
        get
        {
            return fc.m_Animator.GetBool(m_DamagedFall);
        }
        set
        {
            fc.m_Animator.SetBool(m_DamagedFall, value);
        }
    }
    public bool hitStunned
    {
        get
        {
            return fc.m_Animator.GetBool(m_HitStunned);
        }
        private set
        {
            fc.m_Animator.SetBool(m_HitStunned, value);
        }
    }

    public override bool EngineInputDetected
    {
        get
        {
            return Jab || JumpInput;
        }
    }

    protected bool Jab
    {
        get
        {
            return fc.playerInput.GetButtonValue(SmashInputManagerActions.Jab);
        }
    }

    protected bool JumpInput
    {
        get
        {
            return fc.playerInput.GetButtonValue(SmashInputManagerActions.Jump);
        }
    }

    public override void OnMoveIdSet(string moveId)
    {
        string moveName = fighterData.GetMove(moveId)?.name;
        if (!String.IsNullOrWhiteSpace(moveName) && moveName.Contains("Roll"))
        {
            TriggerRoll();
        }
    }

    public virtual bool isInGuard { 
       // get { return state == FighterStateEnum.Guard; } 
        get { return fc.fighterState is Sm4shFighterStates.Guard; } 

    }

    public FighterShield shield  {get; set;}
    [SerializeField]
    float _shieldDepletionPerFrame = 0.13f;
    public float shieldDepletionPerFrame {get {return _shieldDepletionPerFrame;}}

    [SerializeField]
    float _shieldRegenerationPerFrame = 0.08f;
    public float shieldRegenerationPerFrame {get {return _shieldRegenerationPerFrame;}}
     
    [SerializeField] float _maxShield = 50f;
    public float maxShield { get { return _maxShield; } }
    public Action OnHit { get; set; }

    [ReadOnly]
    public int lastHitFrame;

    public override void RegisterAttack(Move move, FighterController attacker, FighterController target, HitPoint hitPoint )
    {
        if (hitDetectedHash.HasValue && hitDetectedHash == currentStateHash) 
        {
            return;
        }

        if (move != null)
        {
//           lastRequestedTime = Time.time;
            target.attackEngine.RegisterWasHit(hitPoint, move);
            onHitAnimatorCallback?.Invoke();
            move.chargedFrames = 0;//TODO - up this as needed
            GeniousSocketManager.instance.SendKnockbackRequest(new KnockbackRequest(move,move.chargedFrames, attacker, target));
        }
    }

    static bool calculatorStarted;

    void StartCalculator()
    {
        if (calculatorStarted)
        {
            return;
        }
       
        DamageCalculatorManager.instance.calculatorBinary = binary.WriteBinaryToPersistentDataPath();
        calculatorStarted = true;
    }
    protected override void Start()
    {
        base.Start();
        StartCalculator();
        fc.SetFighterState<Sm4shFighterStates.Normal>();

        GenericCoroutineManager.instance.RunAfterFrame(() => {
            shield = fc.GetComponentInChildren<FighterShield>(true);
            if (shield == null)
            {
                Debug.LogError("No shield fond.");
            }
            else
            {
                shield.InitializeShield(maxShield, TriggerShieldBreak, false);
            }
        });
    }

    bool shieldButtonPressed;
    public void SetShield(bool val)
    {
        Debug.Log("Block: " + SmashInputManagerActions.Block.name + " - " + SmashInputManagerActions.Pause.name);
        shieldButtonPressed = val;
       // FighterStateEnum toSet = state;
        if (val && !CheckRoll() && fc.fighterState is Sm4shFighterStates.Normal /*state == FighterStateEnum.Normal*/ && fc.m_IsGrounded)
        {
            fc.SetFighterState<Sm4shFighterStates.Guard>();
//            toSet = FighterStateEnum.Guard;
        }
        else if (!val && fc.fighterState is Sm4shFighterStates.Guard)// state == FighterStateEnum.Guard)//Guard dropped
        {
            fc.SetFighterState<Sm4shFighterStates.Normal>();
           // toSet = FighterStateEnum.Normal;
        }

       // shield.SetActive(val);
       // state = toSet;
    }

    public bool CheckRoll()
    {
        return (fc.locomotion.movementInputMagnitude > 0.6f);/* TODO - make this number "RollThreshold'
        {
            return true;
        }
        return false;*/
    }

    //float lastRequestedTime;

    public override void HandleKnockbackResponse(KnockBackResponse knockback, FighterController damageSource, Move attack)
    {
       // Debug.Log("Ping: " + (Time.time - lastRequestedTime));
        percent += knockback.damage;
        ResolveFreezeFrames(knockback.damage);

        Debug.Log(GeniousSettings.fixedFrame + ".) Hit with " + attack.moveName + " for " + knockback.damage + "% (Currently " + percent + "\n" + knockback);

        if (knockbackCoroutine != null)
        {
            StopCoroutine(knockbackCoroutine);
        }
        knockbackCoroutine = StartCoroutine(Knockback(knockback, damageSource));
    }

    int? currentStateHash;
    int? hitDetectedHash;
    public void SetCurrentStateHash(int hash)
    {
        currentStateHash = hash;
    }

    public override void MoveCompleted(Move move)
    {
        currentStateHash = null;
        hitDetectedHash = null;
    }

    void TriggerShieldBreak()
    {
        fc.SetFighterState<Sm4shFighterStates.Stunned>();
    }

    Coroutine knockbackCoroutine;
    [ReadOnly]
    public Vector2 diStick;

    IEnumerator HandleAfterLandingCompleted()
    {
        while (!fc.m_IsGrounded)
        {
            yield return new WaitForFixedUpdate();
        }

        if (damagedFall)
        {
            SetFloored();
            damagedFall = false;
        }
        else
        {
            SetNormalState();
        }

        if (hitStunned)
        {
            CancelHitStun();
        }
    }

    public void TriggerRoll()
    {
        fc.SetFighterState<Sm4shFighterStates.Roll>();
        StartCoroutine(Roll());
    }
    public void AdjustShieldHealth(float toAdjust)
    {
        shield.AdjustShieldHealth(toAdjust);
    }

    protected virtual IEnumerator Knockback(KnockBackResponse knockback, FighterController damageSource)
    {
        fc.inKnockback = true;
        yield return StartCoroutine(KnockbackCalculation(knockback, damageSource));
        fc.inKnockback = false;
    }

    
    public void Kill()
    {
        if (fc.fighterState is Sm4shFighterStates.Dead)// == FighterStateEnum.Dead)
        {
            return;
        }

        fc.SetFighterState<Sm4shFighterStates.Dead>();
       // state = FighterStateEnum.Dead;
        if (fc?.locomotion?.m_VerticalSpeed > 0)
        {
            fc.locomotion.SetMinFallSpeed();
            //fc.locomotion.m_VerticalSpeed = minFallSpeed;
        }
        GeniousEffectManager.instance.GenerateDeathEffect(transform.position, () => {
            Death();
            fc.transform.position = GeniousSettings.instance.GetRespawnLocation();
            StopAllCoroutines();
            fc.StopAllCoroutines();
            fc.m_Animator.Rebind();
        });
    }

    Coroutine hitStunCancelCoroutine;
    protected virtual IEnumerator KnockbackCalculation(KnockBackResponse knockback, FighterController damageSource)
    {
        if (fc.invulnerable)
        {
            yield break;
        }
        var startPosition = transform.position;
        var direction = transform.position - damageSource.transform.position;
        var angle = Vector3.SignedAngle(Vector3.right, direction, Vector3.up);
        if (isInGuard)
        {

            AdjustShieldHealth(-knockback.shield_damage);
            yield break;
        }
        Vector3 ogDamageSource = damageSource.transform.position;
        if (hitStunCancelCoroutine != null)
        {
            StopCoroutine(hitStunCancelCoroutine);
        }
        hitStunCancelCoroutine = StartCoroutine(TrackHitStun(knockback.hitstun, knockback.airdodge_hitstun_cancel, knockback.aerial_hitstun_cancel));

        if (knockback.reeling)
        {
            TriggerReeling();
        }
        else if (knockback.tumble)
        {
            TriggerTumbling();
        }

        damagedFall = knockback.reeling || knockback.tumble;
        LaunchTrajectory lastTrajectory = new LaunchTrajectory { position = new Position()};// lastTraj
        foreach (var trajectory in knockback.launch_trajectory)
        {
            var launchSpeed = Mathf.Sqrt(Mathf.Pow((float) knockback.horizontal_launch_speed, 2) + Mathf.Pow((float)knockback.vertical_launch_speed, 2)); //Include gravity boost to the new launch speed (yes this only happens when stick isn't on neutral)
            var scaledStick = diStick * 127;
            var diAngle = DI(scaledStick, new Vector2((float) knockback.horizontal_launch_speed, (float)knockback.vertical_launch_speed), launchSpeed, .17f);
            var distance = Mathf.Sqrt(Mathf.Pow(trajectory.vector.x, 2) + Mathf.Pow(trajectory.vector.y, 2));//Vector2.Distance(trajectory.vector, Vector2.zero);
            var angleToAdjust = diAngle - knockback.launch_angle;

            var a = Mathf.Atan2(trajectory.position.y, trajectory.position.x) * Mathf.Rad2Deg;
            var newAngle = Mathf.Deg2Rad * (a + angleToAdjust);
            if (distance != 0 && !float.IsNaN(distance))
            {
                var oldTrajectory = trajectory.vector;
                trajectory.vector = new Vector2(Mathf.Cos(newAngle) * distance, Mathf.Sin(newAngle) * distance);
             //   Debug.Log((Mathf.Rad2Deg * newAngle) + " from " + a + " + " +  angleToAdjust + " and " + distance + " -> " + trajectory.vector + " from " + oldTrajectory);
            }
            /*REVISION  7e6744b60bef01ab2aee8dae32257d39b3cdb6c9
                        var a = Mathf.Atan2(trajectory.position.y - lastTrajectory.position.y, trajectory.position.x - lastTrajectory.position.x) * Mathf.Rad2Deg;

                        var distance = Vector2.Distance(trajectory.vector, lastTrajectory.vector);
                        var newAngle = a + angleToAdjust;
                      //  Debug.Log(trajectory.vector + " before - " + distance + "(new Angle: " + newAngle + " from " + diAngle + " - " + knockback.launch_angle + " and " + a);
                        if (distance != 0)
                        {
                       //     trajectory.vector = new Vector2(Mathf.Cos(newAngle) * distance, Mathf.Sin(newAngle) * distance);
                        }
                       // Debug.Log(trajectory.vector + " after"); 
            */
            var startRotation = transform.rotation;
            Vector3 flatDirection = -transform.forward;
            //flatDirection.x = 90; 
            transform.LookAt(ogDamageSource);
            var translation = -transform.forward * trajectory.position.x;
            translation.y = trajectory.position.y;
            Vector3 lastPosition = transform.position;

            fc.locomotion.MoveCharacter(startPosition + translation * GeniousSettings.instance.knockbackScalar - transform.position);
            Vector3 newDirection = transform.position - lastPosition;
            transform.rotation = startRotation;
            yield return new WaitForFixedUpdate();
            lastTrajectory = trajectory;
        }

        StartCoroutine(HandleAfterLandingCompleted());
    }

    IEnumerator TrackHitStun(int hitStunLength, int hitStunAirDodgeCancelLength, int hitStunAerialCancelLength)
    {
        int startFrame = GeniousSettings.fixedFrame;
        while (hitStunLength-- > 0)
        {
            if (hitStunned == false)
            {
                yield break;
            }
            hitStunAerialCancellable = startFrame + hitStunAerialCancelLength <= GeniousSettings.fixedFrame;
            hitStunAirDodgeCancellable = startFrame + hitStunAirDodgeCancelLength <= GeniousSettings.fixedFrame;

            yield return new WaitForFixedUpdate();
            
        }
        CancelHitStun(true);
    }

    public void CancelHitStun()
    {
        CancelHitStun(false);
    }

    public void CancelHitStun(bool allowFlooredLanding)
    {
        Debug.Log(GeniousSettings.fixedFrame + ".) Cancelling hit stun" );
        if (!allowFlooredLanding)
        {
            damagedFall = false;
        }
        hitStunned = false;
        //damaged = false;
    }

    public virtual void TriggerTumbling()
    {

    }

    public virtual void TriggerReeling()
    {

    }

    protected virtual void SetFloored()
    {
        fc.SetFighterState<Sm4shFighterStates.Floored>();
        GenericCoroutineManager.instance.RunInSeconds(GeniousSettings.instance.autoGetUpDuration, () =>
        {
            TriggerGetUp();
        });
    }

    void ResolveFreezeFrames(float damage)
    {
        Frame numFrames = GetFreezeFrameValue(damage);
        if (freezeFramesCoroutine != null)
        {
            float secondsCompletedSoFar = Time.realtimeSinceStartup - freezeFrameStarted.Value;
            StopCoroutine(freezeFramesCoroutine);
            if (numFrames.seconds <= secondsCompletedSoFar)
            {
                FinishFreezeFrames();
                return;
            }
            else
            {
                numFrames = numFrames.value - Mathf.RoundToInt(secondsCompletedSoFar * GeniousSettings.instance.targetFrameRate);
            }
        }

        FreezeFrames(numFrames);
    }

    Coroutine freezeFramesCoroutine;
    float? freezeFrameStarted;
    void FreezeFrames(Frame numFrames)
    {
        Time.timeScale = 0f;
        freezeFrameStarted = Time.realtimeSinceStartup;
        freezeFramesCoroutine = GenericCoroutineManager.instance.RunInSecondsRealtime(numFrames.seconds, () =>
        {
            FinishFreezeFrames();
        });
    }

    void FinishFreezeFrames()
    {
        Time.timeScale = 1f;
        freezeFramesCoroutine = null;
        freezeFrameStarted = null;
    }

    Frame GetFreezeFrameValue(float damage)
    {
        var modifierMultipler = 1f;
        Frame numFramesToWait = Mathf.FloorToInt((damage * GeniousSettings.instance.freezeFramesMultiplier + GeniousSettings.instance.freezeFramesAdditive) * modifierMultipler);
        return numFramesToWait;
        // return numFramesToWait.seconds;
    }

    
    IEnumerator Roll()
    {
        var m = fc.GetCurrentMove();
        string[] split = m.unparsedMove.HitboxActive.Split('-');
        int firstFrame = Convert.ToInt32(split[0].Trim());
        int lastFrame = Convert.ToInt32(split[1].Trim());

        for (int i = 0; i < Convert.ToInt32(m.faf) - 1; i++)
        {
            fc.invulnerable = i >= firstFrame && i <= lastFrame;
            yield return new WaitForEndOfFrame();
        }

        SetNormalState();

        if (fc.playerInput.GetButtonValue(SmashInputManagerActions.Block))
        {
            SetShield(true);
        }
    }

    bool rightStickSmash = true;
    public override void RegisterInputCallbacks(PlayerInput fighterInput)
    {
        fighterInput.AppendAction<bool>(SmashInputManagerActions.NextCharacter, SwapCharacters);


        fighterInput.AppendAction<float>(SmashInputManagerActions.MovementVertical, VerticalDI);
        fighterInput.AppendAction<float>(SmashInputManagerActions.MovementHorizontal, HorizontalDI);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.GetUp, TriggerGetUp);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.Block, SetShield);
        fighterInput.AppendComboAction(SetUpTilt, SmashInputManagerActions.Jab, SmashInputManagerActions.Jump);
        fighterInput.AppendComboAction(SetUpSmash, SmashInputManagerActions.Smash, SmashInputManagerActions.Jump);
        fighterInput.AppendComboAction(SetUpSpecial, SmashInputManagerActions.Special, SmashInputManagerActions.Jump);
        fighterInput.AppendComboAction(SetDownTilt, SmashInputManagerActions.Jab, SmashInputManagerActions.Smash);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.Special, OnSpecial);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.Smash, OnSmash);
        if (rightStickSmash)
        {
            fighterInput.AppendComboAction(SetDownSmash, SmashInputManagerActions.Smash, SmashInputManagerActions.Special);
            fighterInput.AppendAction<Vector2>(SmashInputManagerActions.AltAxisHorizontal, (val) =>
            {
                SetAltAxis(val, m_DownSmash, m_ForwardSmash);
            }, SmashInputManagerActions.AltAxisVertical);
        }
        else
        {
            fighterInput.AppendComboAction(SetDownSpecial, SmashInputManagerActions.Smash, SmashInputManagerActions.Special);
            fighterInput.AppendAction<Vector2>(SmashInputManagerActions.AltAxisHorizontal, (val) =>
            {
                SetAltAxis(val, m_DownSpecial, m_ForwardSpecial);
            }, SmashInputManagerActions.AltAxisVertical);
        }
        fighterInput.AppendComboAction(SetDownSmash, SmashInputManagerActions.Special, SmashInputManagerActions.Jump);

        SetupAttackCallback(SmashInputManagerActions.Jab);
        SetupAttackCallback(SmashInputManagerActions.Smash);
        SetupAttackCallback(SmashInputManagerActions.Special);
        SetupAttackCallback(SmashInputManagerActions.Block);
    }

    bool _attackDetected;
    public override bool attackDetected
    {
        get
        {
            return _attackDetected;
        }
        protected set
        {
            _attackDetected = value;
            fc.m_Animator.SetBool(m_AttackDetected, value);
        }
    }
    HashSet<InputAction> attackDetectedActions = new HashSet<InputAction>();
    void SetupAttackCallback(InputAction action)
    {
        attackDetectedActions.Add(action);
        fc.playerInput.AppendAction<bool>(action, OnAttackButton);
    }

    void OnAttackButton(bool val)
    {
        if (val)
        {
            attackDetected = true;
            fc.SetTrigger(m_AttackStarted);
        }
        else
        {
            bool attackStillDetected = false;
            foreach (var a in fc.activeButtons)
            {
                if (attackDetectedActions.Contains(a.Key))
                {
                    attackStillDetected = true;
                    break;
                }
            }
            if (!attackStillDetected)
            {
                attackDetected = false;
            }
        }
    }

    bool rightStickAllowed = true;
    bool rightStickHorizontalAllowed = true;
    void SetAltAxis(Vector2 axis, int? verticalAction = null, int? horizontalAction = null)
    {
        float cutoff = GeniousSettings.instance.rightStickMagnitudeCutoff;
        if (rightStickAllowed)
        {
            if (axis.y >= cutoff && verticalAction.HasValue)
            {
                fc.SetTrigger(verticalAction.Value);
                rightStickAllowed = false;
            }
            else if (horizontalAction.HasValue && axis.x >= cutoff)
            {
                fc.SetTrigger(horizontalAction.Value);
            }
        }
        else if (axis.y < cutoff && axis.x < cutoff)
        {
            rightStickAllowed = true;
        }
    }

    void OnSpecial(bool val)
    {
        if (val)
        {
            DirectionalAttack(m_ForwardSpecial);
        }
    }

    void OnSmash(bool val)
    {
        if (val)
        {
            DirectionalAttack(m_ForwardSmash);
        }
    }

    void DirectionalAttack(int attackHash)
    {
        if (fc.locomotion.movementInputMagnitude > GeniousSettings.instance.directionalAttackInputMagnitudeCutoff)
        {
            SetAttackTrigger(attackHash);
        }
    }

    void SetUpTilt()
    {
        fc.m_Animator.SetBool("Tilt", true);
        SetAttackTrigger(m_UpTilt);
    }

    void SetDownTilt()
    {
        fc.m_Animator.SetBool("Tilt", true);
        SetAttackTrigger(m_DownTilt);
    }

    void SetUpSmash()
    {
        fc.m_Animator.SetBool("Smash", true);
        SetAttackTrigger(m_UpSmash);
    }

    void SetDownSmash()
    {
        fc.m_Animator.SetBool("Smash", true);
        SetAttackTrigger(m_DownSmash);
    }

    void SetUpSpecial()
    {
        fc.m_Animator.SetBool("Special", true);
        SetAttackTrigger(m_UpSpecial);
    }

    void SetDownSpecial()
    {
        fc.m_Animator.SetBool("Special", true);
        SetAttackTrigger(m_DownSpecial);
    }

    void SetAttackTrigger(int hash)
    {
        attackDetected = true;
        fc.SetTrigger(hash);
    }

    float DI(Vector2 stick, Vector2 launchSpeed, float totalLaunchSpeed, float di)
    {
        if (totalLaunchSpeed < 0.00001f) //There is an if on MSC but it shouldn't happen since it requires tumble for DI to work
            return Mathf.Atan2(launchSpeed.y, launchSpeed.x) * 180 / Mathf.PI;

        if (Mathf.Abs(Mathf.Atan2(launchSpeed.y, launchSpeed.x)) < di) //Cannot DI if launch angle is less than DI angle change param
            return Mathf.Atan2(launchSpeed.y, launchSpeed.x) * 180 / Mathf.PI;

        var X = StickSensibility(stick.x);
        var Y = StickSensibility(stick.y);

        var check = Y * launchSpeed.x - X * launchSpeed.y < 0;

        var variation = Mathf.Abs(X * launchSpeed.y - Y * launchSpeed.x) / totalLaunchSpeed;

        di = di * variation;

        var angle = 0;

        if (check)
            angle = Mathf.RoundToInt((Mathf.Atan2(launchSpeed.y, launchSpeed.x) - di) * 180 / Mathf.PI);
        else
            angle = Mathf.RoundToInt((Mathf.Atan2(launchSpeed.y, launchSpeed.x) + di) * 180 / Mathf.PI);

        if (angle < 0)
            angle += 360;

        return angle;
    }

    
    protected virtual void SetNormalState()
    {
        fc.SetFighterState<Sm4shFighterStates.Normal>();
    }

    public virtual void TriggerTumbling(int hitStunLength)
    {

    }

    public void GetUpComplete()
    {
        SetNormalState();
    }

    public void TriggerGetUp()
    {
        if (fc.fighterState is Sm4shFighterStates.Floored)
        {
            fc.SetFighterState<Sm4shFighterStates.GettingUp>();
        }
    }

    public void TriggerGetUp(bool val)
    {
        if (val)
        {
            TriggerGetUp();
        }
    }

    //Transform diTarget;
    ADamageable diTarget;

    void HorizontalDI(float value)
    {
        if (!fc.locomotion is ITargetable)
        {
            return;
        }

        var currentTarget = ((ITargetable) fc.locomotion).currentTarget;

        if (currentTarget != null)
        {
            diTarget = currentTarget;
        }
        else if (diTarget == null)
        {
            diTarget = ((ITargetable) fc.locomotion).currentTarget;
        }

        if (diTarget == null)
        {
            return;
        }

        var currentRotation = transform.rotation;
        transform.LookAt(diTarget.transform);
        var directRotation = transform.rotation;
        var deltaAngle = Mathf.DeltaAngle(transform.eulerAngles.y, directRotation.eulerAngles.y);// Mathf.DeltaAngle(fc.m_TargetRotation.eulerAngles.y, directRotation.eulerAngles.y);
        deltaAngle = Mathf.Abs(deltaAngle);
        diStick.x = 1 - (2 * deltaAngle / 180);
        transform.rotation = currentRotation;
    }

    void VerticalDI(float value)
    {
        diStick.y = value;
    }

    void SwapCharacters(bool val)
    {
        if (val)
        {
            GeniousEffectManager.instance.GenerateSwapCharacterEffect(transform.position);
            fc.otherChar.playerInput.rewiredPlayer = fc.playerInput.rewiredPlayer;
            fc.otherChar.playerInput.playerNumber = fc.playerInput.playerNumber;
            fc.otherChar.otherChar = fc;
            fc.playerInput.rewiredPlayer = null;
            gameObject.SetActive(false);
            fc.otherChar.transform.position = transform.position;
            //otherChar.percent = percent;
            fc.otherChar.gameObject.SetActive(true);
            Debug.Log("Swapped to " + fc.otherChar.fighterData.displayName);
        }
    }

    float StickSensibility(float value)
    {
        if (value < 24 && value > -24)
            return 0;
        if (value > 118)
            return 1;
        if (value < -118)
            return -1;
        return value / 118;
    }

    public override void Death()
    {
        percent = 0f;
        SetNormalState();
    }

    protected override void FixedUpdate()
    {
        if (!(fc.fighterState is Sm4shFighterStates.Guard))
        {
            SetShield(false);
            shield?.AdjustShieldHealth(shieldRegenerationPerFrame);
        }
    }

    public override void RegisterWasHit(HitPoint hitPoint, Move move)
    {
        if (!(fc.fighterState is Sm4shFighterStates.Guard))
        {
            SetNormalState();
        }

        SetHurtTrigger();
        Debug.Log("Move: " + move);
        
        lastHitFrame = GeniousSettings.fixedFrame;
        hitStunned = true;
        GeniousEffectManager.instance.GenerateHitEffect(hitPoint.position);
        ResolveFreezeFrames(move.base_damage);    
    }
}