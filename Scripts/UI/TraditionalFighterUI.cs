﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TraditionalFighterUI : AGeniousAttackEngineUI
{
    public float time = Mathf.Infinity;
    public Color healthFullColor = Color.green;
    public Color healthEmptyColor = Color.yellow;
    public Color damagedIndicatorBarColor = Color.red;
    public float damageIndicatorTime = 1f;
    [Tooltip("How long the damaged bar will take before syncing with the health bar after waiting the damnageIndicatorTime")]
    public int damageIndicatorLerpFrames = 5;
    public float totalBaseHealth = 120f;
    [Tooltip("Heavier characters will have proportionately more health than lighter characters. The scale will be based around a weight of 100 (~the median)")]
    public float baseWeightForHealthScaling = 100f;
    public TextMeshProUGUI timer;
    public Image player1HealthBar;
    public Image player1RecentlyDamagedBar;
    public TextMeshProUGUI player1Name;
    public Image player2HealthBar;
    public Image player2RecentlyDamagedBar;
    public TextMeshProUGUI player2Name;
    int charsInitialized = 0;

    public override void RegisterPlayer(FighterController fc)
    {
        if (charsInitialized == 0)
        {
            StartCoroutine(HandleTimer());
            StartCoroutine(FollowPlayerData(fc, player1HealthBar, player1Name, player1RecentlyDamagedBar));
        }
        else
        {
            StartCoroutine(FollowPlayerData(fc, player2HealthBar, player2Name, player2RecentlyDamagedBar));
        }

        charsInitialized++;
    }
    IEnumerator FollowPlayerData(FighterController fc, Image healthBar, TextMeshProUGUI playerName, Image playerDamagedBar)
    {
        playerName.text = fc.fighterData.displayName;
        playerDamagedBar.color = damagedIndicatorBarColor;
        float healthMultiplier = fc.fighterData.attributes.weight / baseWeightForHealthScaling;
        float maxHealth = totalBaseHealth * healthMultiplier;
        float lastFillAmount = 1f;
        float timeLastDamaged = float.PositiveInfinity;
        float lastDamagedFillAmount = 1f;
        while (time > 0f)
        {
            var percent = (fc.attackEngine as IPercentable)?.percent;
            if (percent.HasValue)
            {
                healthBar.fillAmount = (maxHealth - percent.Value) / maxHealth;
                healthBar.color = Color.Lerp(healthEmptyColor, healthFullColor, healthBar.fillAmount);
            }

            if (lastFillAmount != healthBar.fillAmount)
            {
                playerDamagedBar.fillAmount = lastDamagedFillAmount = lastFillAmount;
                timeLastDamaged = Time.time;
            }

            if (Time.time > timeLastDamaged + damageIndicatorTime)
            {
                float lerpValue = (Time.time - timeLastDamaged - damageIndicatorTime) / (Time.fixedDeltaTime * damageIndicatorLerpFrames);
                playerDamagedBar.fillAmount = Mathf.Lerp(lastDamagedFillAmount, healthBar.fillAmount, lerpValue);
                if (lerpValue >= 1)
                {
                    timeLastDamaged = float.MaxValue;
                }
                Debug.Log(lastDamagedFillAmount + " to " + healthBar.fillAmount + " at " + lerpValue);
            }

            lastFillAmount = healthBar.fillAmount;
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator HandleTimer()
    {
        while (time > 0f)
        {
            if (time == Mathf.Infinity)
            {
                timer.text = "∞";
            }
            else
            {
                timer.text = Mathf.CeilToInt(time).ToString();
            }
            yield return new WaitForFixedUpdate();
            time -= Time.fixedDeltaTime;
        }
    }
}