﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sm4shUI : AGeniousAttackEngineUI
{
    [SerializeField] FighterPercentUI playerTemplate;
    public override void RegisterPlayer(FighterController fc)
    {
        var playerUi = Instantiate(playerTemplate);
        playerUi.referenceCharacter = fc;
        playerUi.gameObject.SetActive(true);
        playerUi.transform.SetParent(playerTemplate.transform.parent);
    }
}
