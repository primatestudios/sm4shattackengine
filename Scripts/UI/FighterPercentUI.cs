using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FighterPercentUI : MonoBehaviour
{
    public float percentSignScale = 0.4f;
    public TextMeshProUGUI percentText;
    public TextMeshProUGUI nameText;
    public Color finalColor;
    public Color middleColor;
    public float finalColorPercent;
    public Image portrait;
    public FighterController referenceCharacter;
    [Range(0f, 1f)]
    public float screenHeightPercentage;
    Color startColor;
    public IEnumerator Start()
    {
        percentText.enableAutoSizing = false;
        startColor = percentText.color;
        ((RectTransform)transform).sizeDelta = Vector2.one * Screen.height * screenHeightPercentage;
        /*GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            percentText.enableAutoSizing = false;
        });*/
        yield return new WaitForEndOfFrame();
        UpdateValues();

        //portrait.sprite = referenceCharacter.frameData.portrait;
    }
    float? lastPercent;
    public void Update()
    {
        var percent = (referenceCharacter.attackEngine as IPercentable)?.percent;
        if (referenceCharacter != null)
        {
            if (!referenceCharacter.gameObject.activeSelf && referenceCharacter.otherChar != null)
            {
                referenceCharacter = referenceCharacter.otherChar;
                UpdateValues();
            }
            else if (lastPercent != percent)
            {
                UpdateValues();
            }
        }

        lastPercent = percent;
    }

    void UpdateValues()
    {
        var percent = (referenceCharacter.attackEngine as IPercentable)?.percent;
         if (!percent.HasValue)
        {
            return;
        }
        UpdateValues(percent.Value);
    }

    void UpdateValues(float percent)
    {
        nameText.text = referenceCharacter.fighterData.displayName;
        portrait.sprite = referenceCharacter.fighterData.portrait;// characterPortrait;
        percentText.text = Mathf.RoundToInt(percent).ToString() + "<size=" + (percentText.fontSize * percentSignScale) + ">%</size>";
        Color c;
        if (percent > finalColorPercent / 2f)
        {
            c = Color.Lerp(middleColor, finalColor, finalColorPercent / 2 / finalColorPercent);
        }
        else
        {
            c = Color.Lerp(startColor, middleColor, percent/ (finalColorPercent / 2f));
        }
        percentText.color = c;
    }
}
