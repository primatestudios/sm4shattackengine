using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Sm4shFighterStates
{
    public class Normal : AFighterState
    {
        protected override void OnInit()
        {
            if (fc?.locomotion)
            {
                fc.locomotion.canJump = true;

            }
        }

        protected override void OnExit()
        {
            if (fc?.locomotion)
            {
                fc.locomotion.canJump = false;
            }
        }
    }

    public abstract class AHitStunState : AFighterState
    {
        protected override InputActionCallback[] inputActionsToWatch { 
            get {
                return new InputActionCallback[] 
                {
                    InputActionCallback.New<bool>(SmashInputManagerActions.Jump, OnJump)
                };
            }
        }

        bool jumpInput;
        void OnJump(bool val)
        {
            jumpInput = val;
        }

        public override void AfterFighterFixedUpdate()
        {
            GenericCoroutineManager.instance.RunAfterFrame(() => {
                bool hitStunAerialCancellable = fc.attackEngine is IHitStunnable ? ((IHitStunnable)fc.attackEngine).hitStunAerialCancellable : false;
                bool hitStunAirDodgeCancellable = fc.attackEngine is IHitStunnable ? ((IHitStunnable)fc.attackEngine).hitStunAirDodgeCancellable : false;
                if ((hitStunAerialCancellable && (fc.attackEngine.attackDetected || jumpInput))
                    || (hitStunAirDodgeCancellable && fc.playerInput.IsButton(SmashInputManagerActions.AirDodge))
                    || (!fc.inKnockback && fc.m_IsGrounded))
                {
                // Debug.Log(GeniousSettings.fixedFrame + ".) Grounded: " + fc.m_IsGrounded);
                    //                GenericCoroutineManager.instance.RunInFixedUpdateFrames(1, () => { CancelHitStun(); });
                    if (fc.attackEngine is IHitStunnable)
                    {
                        
                        ((IHitStunnable)fc.attackEngine).CancelHitStun();
                    }
                }
            });
        }
    }

    public class Tumbling : AHitStunState
    {

    }

    public class Reeling: AHitStunState
    {

    }

    public class Stunned : AFighterState
    {
        int shieldBreakFramesLeft;    
        //Source: https://www.reddit.com/r/smashbros/comments/879cny/what_is_the_formula_for_how_long_the_shieldbreak/
        IEnumerator MonitorShieldBreak()
        {
            var percent = fc.attackEngine is IPercentable ? ((IPercentable)fc.attackEngine).percent : 100f;
            shieldBreakFramesLeft = Mathf.RoundToInt(GeniousSettings.instance.maxShieldBreakFrames - percent * GeniousSettings.instance.shieldBreakPercentReductionMultiplier);
            while (shieldBreakFramesLeft > 0)
            {
                yield return new WaitForEndOfFrame();
                shieldBreakFramesLeft--;
                //Debug.Log(shieldBreakFramesLeft + " more franmes");
            }

            fc.SetFighterState<Normal>();
        }

        protected override void OnInit()
        {
            fc.locomotion.canMove = false;
        }

        public override void AfterFighterFixedUpdate()
        {
            GenericCoroutineManager.instance.RunAfterFrame(() =>
            {
                if (fc.buttonInputsDetectedThisFrame > 0)
                {
                    Debug.Log("Reducing shield stun by " + (GeniousSettings.instance.shieldBreakFrameReductionPerButtonInput * fc.buttonInputsDetectedThisFrame));
                }

                shieldBreakFramesLeft -= GeniousSettings.instance.shieldBreakFrameReductionPerButtonInput * fc.buttonInputsDetectedThisFrame;
            });
        }
    }

    public class Floored: AFighterState
    {
        public override void AfterFighterFixedUpdate()
        {
            //If we change this inputMagnitude here, we need to update the transitions from "Floored" to "Roll" states in Mecanim
            if (fc.attackEngine.attackDetected || fc.locomotion.movementInputMagnitude > 0.4f)
            {
                if (fc.attackEngine is IFloorable)
                {
                    ((IFloorable)fc.attackEngine).TriggerGetUp();
                }
            }
        }
    }

    public class GettingUp : AFighterState
    {
        public void Complete()
        {
            if (fc.fighterState is GettingUp)
            {
                fc.SetFighterState<Normal>();
            }
        }
    }

    public class Roll : AFighterState
    {

    }

    public class Dead : AFighterState
    {
        protected override void OnInit()
        {
            fc.shouldRunFixedUpdate = false;
        }

        protected override void OnExit()
        {
            fc.shouldRunFixedUpdate = true;
        }
    }
    public class Guard : AFighterState
    {
        protected override void OnInit()
        {
            SetShieldActive(true);
        }

        //TODO - test if this even works? don't know if this is proper or not. At first I didn't put this here so maybe that was for a reason
        protected override void OnExit()
        {
            SetShieldActive(false);
        }
        void SetShieldActive(bool val)
        {
            if (fc.attackEngine is IShieldable)
            {
                ((IShieldable)fc.attackEngine).shield.SetActive(val);
            }
        }
        public override void AfterFighterFixedUpdate()
        {
            if (fc.attackEngine is IShieldable && !(fc.attackEngine is IRollable && ((IRollable)fc.attackEngine).CheckRoll()))
            {
                ((IShieldable)fc.attackEngine).shield.AdjustShieldHealth(-((IShieldable)fc.attackEngine).shieldDepletionPerFrame);
            } 
        }
    }
}