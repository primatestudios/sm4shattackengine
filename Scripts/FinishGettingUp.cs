using System.Collections;
using System.Collections.Generic;
using Sm4shFighterStates;
using UnityEngine;

public class FinishGettingUp : FighterAnimatorScript 
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (controlla.fighterState is GettingUp)
        {
            ((GettingUp)controlla.fighterState).Complete();
        }
//        controlla.TriggerGetUpComplete();
    }
}
